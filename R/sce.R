
revlog_trans <- function(base = 10) {
	require(scales)
	trans <- function(x) -log(x, base)
	inv <- function(x) base^(-x)
	trans_new(
		paste0("revlog-", format(base)), trans, inv,
		log_breaks(base = base), 
		domain = c(1e-100, Inf)
	)
}


#' SingleCellExperiment gene expression plot
#'
#' @param sce  \code{SingleCellExperiment}
#' @export
sce_expr_plot <- function(sce, gene, group_by, stratify_by=NULL, cols=NULL, show.p=TRUE, rasterize=TRUE) {
	g.prop <- sce_prop_plot(sce, gene, group_by, stratify_by, cols=cols, show.p=show.p) +
		theme(axis.text.x = element_blank());
	g.vio <- sce_violin_plot(sce, gene, group_by, stratify_by, cols=cols, show.p=show.p, rasterize=rasterize);

	ggpubr::annotate_figure(
		cowplot::plot_grid(g.prop, g.vio,
			ncol=1, align="v", rel_heights=c(0.6, 1)),
		top = gene
	)
}

sce_violin_plot <- function(sce, gene, group_by, stratify_by=NULL, cols=NULL, show.p=TRUE, max.pts=100, rasterize=TRUE) {
	require(SingleCellExperiment)

	gene.d <- data.frame(
		group = colData(sce)[[group_by]],
		logcount = as.numeric(logcounts(sce[gene, ]))
	);

	if (!is.null(stratify_by)) {
		gene.d$stratum <- colData(sce)[[stratify_by]];
		gene.d$stratified_group <- paste(gene.d$stratum, gene.d$group, sep=".");
	}

	if (is.null(cols)) {
		cols <- pal_npg()(length(levels(gene.d$group)));
	}

	# only consider showing non-zero values
	gene.d$show = gene.d$logcount > 0;

	if (!rasterize) {
		n.show <- sum(gene.d$show);
		if (n.show > max.pts) {
			# randomly hide data points
			gene.d$show[sample(which(gene.d$show), n.show - max.pts)] <- FALSE;
		}
	}

	if (show.p) {
		if (is.null(stratify_by)) {
			p <- kruskal.test(logcount ~ group, gene.d)$p.value;
		} else {
			p <- kruskal.test(logcount ~ stratified_group, gene.d)$p.value;
		}
	}

	summary.d <- group_by(gene.d, group) %>% summarize(mean = mean(logcount));

	if (is.null(names(cols))) {
		names(cols) <- levels(gene.d$group);
	}
	cols2 <- unlist(lapply(cols, function(cc) shade(cc, 0.3)));

	if (rasterize) {
		rasterize.f <- function(x, ...) ggrastr::rasterize(x, dpi=300);
	} else {
		rasterize.f <- identity;
	}

	g <- ggplot(gene.d, aes(x=group, y=logcount, fill=group)) +
		theme_classic() +
		geom_violin(colour=NA) + 
		rasterize.f(geom_jitter(aes(colour=group), width=0.1,
			data=filter(gene.d, show), alpha=0.6)) +
		geom_violin(fill=NA) + 
		geom_point(aes(x=group, y=mean), data=summary.d, shape=23, size=2, fill="white") +
		scale_fill_manual(values=cols, guide="none") +
		scale_colour_manual(values=cols2, guide="none") +
		xlab("") + ylab("log count") +
		theme(
			plot.title = element_markdown(size=10),
			axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1)
		)

	if (!is.null(stratify_by)) {
		g <- g + facet_wrap(~ stratum, nrow=1) +
			theme(strip.background = element_blank());
	}

	if (show.p) {
		g <- g + ggtitle(sprintf("*p* = %s", format(p, digits=2)));
	}

	g
}

sce_prop_plot <- function(sce, gene, group_by, stratify_by=NULL, cols=NULL, show.p=TRUE, threshold=0) {
	require(SingleCellExperiment)

	gene.d <- data.frame(
		group = colData(sce)[[group_by]],
		logcount = as.numeric(logcounts(sce[gene, ]))
	);

	if (!is.null(stratify_by)) {
		gene.d$stratum <- colData(sce)[[stratify_by]];
		gene.d$stratified_group <- paste(gene.d$stratum, gene.d$group, sep=".");
	}

	if (is.null(cols)) {
		cols <- pal_npg()(length(levels(gene.d$group)));
	}

	if (show.p) {
		if (is.null(stratify_by)) {
				p <- with(gene.d, fisher.test(table(group, logcount > threshold), simulate.p.value=TRUE)$p.value);
		} else {
				p <- with(gene.d, fisher.test(
						table(stratified_group, logcount > threshold), simulate.p.value=TRUE)$p.value
				);
		}
	}

	prop <- function(y) {
		r <- prop.test(sum(y > threshold, na.rm=TRUE), sum(!is.na(y)));
		data.frame(
			y = r$estimate * 100,
			ymin = max(0, r$conf.int[1]) * 100,
			ymax = min(1, r$conf.int[2]) * 100
		)
	}

	if (is.null(stratify_by)) {
		summary.d <- group_by(gene.d, group) %>% 
			summarize(prop(logcount));
	} else {
		summary.d <- group_by(gene.d, group, stratum) %>% 
			summarize(prop(logcount));
	}

	g <- ggplot(summary.d, aes(x=group, y=y, ymin=ymin, ymax=ymax, fill=group)) +
		theme_classic() +
		geom_col() + 
		geom_errorbar(width=0.2) +
		scale_fill_manual(values=cols, guide="none") +
		xlab("") + ylab("% expressed") +
		theme(
			plot.title = element_markdown(size=10),
			axis.text.x = element_text(angle = 45, hjust = 1, vjust = 1)
		);
	
	if (!is.null(stratify_by)) {
		g <- g + facet_wrap(~ stratum, nrow=1) +
			theme(strip.background = element_blank());
	}

	if (show.p) {
		g <- g + ggtitle(sprintf("*p* = %s", format(p, digits=2)));
	}

	g 
}

#' Reduced dimensionality plot
#'
#' @param sce     \code{SingleCellExperiment} object
#' @param dimred  type of dimensionality reduction data (e.g. UMAP, TSNE)
#' @export
rdim_plot <- function(sce, dimred=NULL, order_by=NULL, group_by=NULL,
	colour_by=NULL, shape_by=NULL, size_by=NULL, text_by=NULL,
	point_size=0.5, point_alpha=0.6, 
	colour_pal=NULL, rasterize=TRUE,
	...) {
	require(scater)

	if (is.null(order_by)) {
		order_by <- colour_by;
	}

	if (!is.na(order_by)) {
		if (order_by %in% names(colData(sce))) {
			idx <- order(colData(sce)[order_by]);
		} else {
			idx <- order(counts(sce)[order_by, ]);
		}
		sce <- sce[, idx];
	}
	
	g <- plotReducedDim(sce, dimred=dimred,
		colour_by=colour_by, shape_by=shape_by, size_by=size_by, text_by=text_by,
		point_size=point_size, point_alpha=point_alpha, ...);

	if (!is.null(colour_pal) && colour_pal == "expression") {
		g <- g + scale_colour_gradientn(colours=c("grey90", "#FD8D3C", "#E31A1C", "#800026"), name=colour_by);
	}
	
	if (rasterize) {
		g <- ggrastr::rasterize(g, dpi=300);
	}

	g <- g + coord_fixed() +
		theme(
			legend.position="bottom", legend.box = "horizontal",
			strip.background = element_blank()
		);

	if (!is.null(group_by)) {
		g <- g + facet_wrap(colData(sce)[[group_by]], ncol=1);
	}

	g
}

#' Compute gene expression log fold change
#'
#' @param sce  \code{SingleCellExperiment} object
#' @param groups  group factor (first level is the reference)
#' @param genes   subset of genes
#' @return \code{numeric} vector of fold change values
sce_gene_logfc <- function(sce, groups, genes=NULL) {
	if (is.factor(groups)) {
	    group.levels <- levels(groups);
	} else {
	    group.levels <- sort(unique(groups));
	}
	names(group.levels) <- group.levels;
	means <- lapply(group.levels, function(g) {
		idx <- which(groups == g);
		if (is.null(genes)) {
			rowMeans(logcounts(sce)[, idx, drop=FALSE])
		} else {
			rowMeans(logcounts(sce)[genes, idx, drop=FALSE])
		}
	});
	means[[2]] - means[[1]]
}

