bound <- function(xs, lim) {
	lower <- min(lim);
	upper <- max(lim);
	ifelse(xs < lower, lower,
		ifelse(xs > upper, upper, xs)
	)
}

sanitize <- function(x) {
	tolower(gsub(".", "-", x, fixed=TRUE))
}

capitalize <- function(s) {
	ifelse(is.na(s),
		NA, 
		paste0(toupper(substring(s, 1, 1)), tolower(substring(s, 2)))
	)
}

get_gene_idx <- function(genes, sce) {
	na.omit(match(tolower(genes), tolower(rowData(sce)$Symbol)))
}

tint <- function(x, fac) {
	x <- c(grDevices::col2rgb(x))
	x <- (x + (255 - x) * fac) / 255
	grDevices::rgb(x[1], x[2], x[3])
}

shade <- function(x, fac) {
	x <- c(grDevices::col2rgb(x))
	x <- x * (1 - fac) / 255
	grDevices::rgb(x[1], x[2], x[3])
}

